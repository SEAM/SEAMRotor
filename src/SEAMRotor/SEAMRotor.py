__author__ = 's127504'

from decimal import *

import matplotlib.pyplot as plt
from math import pow
import numpy as np
from scipy.optimize import minimize

from openmdao.main.api import Component
from openmdao.lib.datatypes.api import Float, Array, Bool, Int

class SEAMBladeStructure(Component):

    blade_sections = Int(iotype='in', desc='number of sections along blade')
    lifetime_cycles = Float(1.e7, iotype='in', desc='Equivalent lifetime cycles')

    wohler_exponent_blade_flap = Float(iotype='in', desc='Blade flap fatigue Wohler exponent')
    PMtarget_blades = Float(1., iotype='in', desc='')

    rotor_diameter = Float(iotype='in', units='m', desc='Rotor diameter')
    MaxChordrR = Float(iotype='in', units='m', desc='Spanwise position of maximum chord')

    blade_root_flap_max = Float(iotype='in', units='kN*m', desc='Max blade root flapwise moment')
    blade_root_edge_max = Float(iotype='in', units='kN*m', desc='Max blade root edgewise moment')
    blade_root_flap_leq = Float(iotype='in', units='kN*m', desc='Blade root flapwise lifetime eq. moment')
    blade_root_edge_leq = Float(iotype='in', units='kN*m', desc='Blade root edgewise lifetime eq. moment')

    tif_blade_root_flap_ext = Float(1., iotype='in', desc='Technology improvement factor flap extreme')
    tif_blade_root_edge_ext = Float(1., iotype='in', desc='Technology improvement factor edge extreme')
    tif_blade_root_flap_fat = Float(1., iotype='in', desc='Technology improvement factor flap LEQ')

    sc_frac_flap = Float(iotype='in', desc='spar cap fraction of chord')
    sc_frac_edge = Float(iotype='in', desc='spar cap fraction of thickness')

    safety_factor_blade = Float(iotype='in', desc='Blade loads safety factor')
    stress_limit_extreme_blade = Float(iotype='in', units='MPa', desc='Blade ultimate strength')
    stress_limit_fatigue_blade = Float(iotype='in', units='MPa', desc='Blade fatigue strength')

    AddWeightFactorBlade = Float(iotype='in', desc='Additional weight factor for blade shell')
    blade_material_density = Float(iotype='in', units='kg/m**3', desc='Average density of blade materials')

    blade_mass = Float(iotype = 'out', units = 'kg', desc = 'Blade mass' )
    blade_root_diameter = Float(iotype = 'out', units = 'm', desc = 'blade root chord') # 07/09/2015 added for HubSE model

    def execute(self):

        volumen = 0.
        r = np.linspace(0, self.rotor_diameter/2., self.blade_sections)
        C = np.zeros(r.shape)
        thick = np.zeros(r.shape)
        blade_root_diameter = (self.rotor_diameter/2.)/25.
        if blade_root_diameter < 1:
            blade_root_diameter = 1

        MaxChord = 0.001*(self.rotor_diameter/2.)**2-0.0354*(self.rotor_diameter/2.)+1.6635  #Empirical
        if MaxChord < 1.5:
            blade_root_diameter = 1.5

        self.blade_root_diameter = blade_root_diameter

        for i in range(self.blade_sections):

            # Calculating the chord and thickness
            if r[i] > (self.MaxChordrR*self.rotor_diameter/2.):
                C[i] = MaxChord - (MaxChord)/((self.rotor_diameter/2.) - \
                                   self.MaxChordrR*self.rotor_diameter/2.)*(r[i] - \
                                  (self.MaxChordrR*self.rotor_diameter/2.))
                thick[i] = 0.4*MaxChord - \
                          (0.4*MaxChord-0.05*MaxChord)/(self.rotor_diameter/2. - \
                          self.MaxChordrR*self.rotor_diameter/2.)*(r[i] - \
                          self.MaxChordrR*self.rotor_diameter/2.)

                if thick[i]<0.001:
                    thick[i] = 0.001
                if C[i]<0.001:
                    C[i] = 0.001
            else:
                C[i] = MaxChord-(MaxChord)/((self.rotor_diameter/2.) - \
                       self.MaxChordrR*(self.rotor_diameter/2.)) * \
                       (self.MaxChordrR*(self.rotor_diameter/2.)-r[i])
                thick[i] = blade_root_diameter

                if thick[i]<0.001:
                    thick[i] = 0.001
                if C[i]<0.001:
                    C[i] = 0.001

        self.thick = thick
        self.chord = C

        # Calculating load flapwise extreme
        Mext_flap = (self.blade_root_flap_max
                    -1.75*self.blade_root_flap_max*r/(self.rotor_diameter/2.) \
                    +0.75*self.blade_root_flap_max*r/(self.rotor_diameter/2.) * r / \
                                                     (self.rotor_diameter/2.))*self.tif_blade_root_flap_ext

        # Calculating load edgewise extreme
        Mext_edge = (self.blade_root_edge_max-1.75*self.blade_root_edge_max*r/(self.rotor_diameter/2.) + \
                    0.75*self.blade_root_edge_max*r/(self.rotor_diameter/2.) * r / \
                    (self.rotor_diameter/2.))*self.tif_blade_root_edge_ext

        # Calculating load flapwise fatigue
        Mfat_flap = (self.blade_root_flap_leq-1.75*self.blade_root_flap_leq*r/(self.rotor_diameter/2.) + \
                    0.75*self.blade_root_flap_leq*r/(self.rotor_diameter/2.)*r / \
                    (self.rotor_diameter/2.))*self.tif_blade_root_flap_fat

        # Calculating load edgewise fatigue
        Mfat_edge = (self.blade_root_edge_leq-1.75*self.blade_root_edge_leq*r/(self.rotor_diameter/2.) + \
        0.75*self.blade_root_edge_leq*r/(self.rotor_diameter/2.)*r/(self.rotor_diameter/2.))*self.tif_blade_root_edge_ext

        self.Mext_flap = Mext_flap
        self.Mext_edge = Mext_edge
        self.Mfat_flap = Mfat_flap
        self.Mfat_edge = Mfat_edge

        # Calculating thickness flapwise extreme
        text_flap = np.zeros(self.blade_sections)
        for i in range(self.blade_sections):
            norm = self.solve_text_flap(0.01, C[i], thick[i], Mext_flap[i], 1.)
            res = minimize(self.solve_text_flap, 0.01, args = (C[i], thick[i], Mext_flap[i], norm), bounds=[(1.e-6, 0.5)], method = 'SLSQP', tol = 1.e-8)
            text_flap[i] = res['x']

            if not res['success']: print 'WARNING enter text_flap', i,res

        # Calculating thickness edgewise extreme
        text_edge = np.zeros(self.blade_sections)
        for i in range(self.blade_sections):
            norm = self.solve_text_edge(0.01, C[i], thick[i], Mext_edge[i], 1.)
            res = minimize(self.solve_text_edge, 0.01, args = (C[i], thick[i], Mext_edge[i], norm), bounds=[(1.e-6, 0.5)], method = 'SLSQP', tol = 1.e-8)
            text_edge[i] = res['x']
            if not res['success']: print 'WARNING solve_text_edge', i,res

        # Calculating thickness flapwise fatigue
        tfat_flap = np.zeros(self.blade_sections)
        for i in range(self.blade_sections):

            norm = self.solve_tfat_flap(0.01, C[i], thick[i], Mfat_flap[i], 1.)
            res = minimize(self.solve_tfat_flap, 0.01, args = (C[i], thick[i], Mfat_flap[i], norm), bounds=[(1.e-6, 0.5)], method = 'SLSQP', tol = 1.e-8)
            tfat_flap[i] = res['x']
            if not res['success']: print 'WARNING solve_tfat_flap', i, res

        # Calculating thickness edgewise fatigue
        tfat_edge = np.zeros(self.blade_sections)
        for i in range(self.blade_sections):
            norm = self.solve_tfat_edge(0.01, C[i], thick[i], Mfat_edge[i], 1.)
            res = minimize(self.solve_tfat_edge, 0.01, args = (C[i], thick[i], Mfat_edge[i], norm), bounds=[(1.e-6, 0.5)], method = 'SLSQP', tol = 1.e-8)
            tfat_edge[i] = res['x']
            if not res['success']: print 'WARNING solve_tfat_edge', i,res

        self.text_flap = text_flap
        self.text_edge = text_edge
        self.tfat_flap = tfat_flap
        self.tfat_edge = tfat_edge
        self.r = r

        # Calculating the final thickness for flap and edge direction
        tfinal_flap = np.maximum(text_flap, tfat_flap)
        tfinal_edge = np.maximum(text_edge, tfat_edge)
        self.tfinal_flap = tfinal_flap
        self.tfinal_edge = tfinal_edge

        self.volume = np.trapz(2 * (self.sc_frac_flap * C * tfinal_flap + \
                                    self.sc_frac_edge * thick * tfinal_edge), r)
        self.volume *= self.AddWeightFactorBlade
        self.blade_mass = self.blade_material_density * self.volume

        print 'blade mass', self.blade_mass

    # Defining functions in order to solve for the thickness

    # Solving for t in flap direction, extremes
    def solve_text_flap(self, t, C, thick, Mext_flap, norm):
        Ine = (2./3.)*self.sc_frac_flap*C*t**3-self.sc_frac_flap*C*thick*t**2+self.sc_frac_flap*C*thick**2/2.*t
        W = Ine/(thick/2.)
        sext = self.safety_factor_blade*1e3*Mext_flap/W/1e6
        # print 'text_flap', abs(sext - self.stress_limit_extreme_blade)
        return abs(sext - self.stress_limit_extreme_blade) / norm

    #Solving for t in edge direction, extremes
    def solve_text_edge(self, t, C, thick, Mext_edge, norm):
        Ine = (2/3.)*self.sc_frac_edge*thick*t**3-self.sc_frac_edge*thick*C*t**2+self.sc_frac_edge*thick*C**2/2.*t
        W = Ine/(C/2.);
        sext = self.safety_factor_blade*1.e3*Mext_edge/W/1.e6
        return abs(sext - self.stress_limit_extreme_blade) / norm

    # Solving for t in flap direction, fatigue
    def solve_tfat_flap(self, t, C, thick, Mfat_flap, norm):
        Ine = (2/3.)*self.sc_frac_flap*C*t**3-self.sc_frac_flap*C*thick*t**2+self.sc_frac_flap*C*thick**2/2.*t
        W = Ine/(thick/2.)
        sfat = np.maximum(1.e-6, self.safety_factor_blade*1.e3*Mfat_flap/ W / 1.e6)
        PM = self.lifetime_cycles/(pow(10, (self.stress_limit_fatigue_blade - self.wohler_exponent_blade_flap*np.log10(sfat))))
        return abs(PM - self.PMtarget_blades) / norm

    # Solving for t in edge direction, fatigue
    def solve_tfat_edge(self, t, C, thick, Mfat_edge, norm):
        Ine = (2/3.)*self.sc_frac_edge*thick*t**3-self.sc_frac_edge*thick*C*t**2+self.sc_frac_edge*thick*C**2/2.*t
        W = Ine/(C/2.)
        sfat = np.maximum(1.e-6, self.safety_factor_blade*1.e3*Mfat_edge/W/1.e6)
        PM = self.lifetime_cycles/(pow(10, (self.stress_limit_fatigue_blade - self.wohler_exponent_blade_flap*np.log10(sfat))))
        return abs(PM - self.PMtarget_blades) / norm

    def plot(self, fig):
        """
        function to generate Bokeh plot for web GUI.

        Also callable from an ipython notebook

        parameters
        ----------
        fig: object
            Bokeh bokeh.plotting.figure object

        returns
        -------
        fig: object
            Bokeh bokeh.plotting.figure object
        """
        try:
            # formatting
            fig.title = 'Blade spar cap thickness'
            fig.xaxis[0].axis_label = 'Radius [m]'
            fig.yaxis[0].axis_label = 'Spar cap thickness [m]'

            # fatigue, ultimate and final thickness line plots
            # fig.line(self.r, self.self.text_flap, line_color='orange',
            #                             line_width=3,
            #                             legend='Extreme flap')
            # fig.line(self.r, self.text_edge, line_color='green',
            #                             line_width=3,
            #                             legend='Extreme edge')
            # fig.line(self.r, self.tfat_flap, line_color='blue',
            #                             line_width=3,
            #                             legend='Fatigue flap')
            fig.line(self.r, self.tfinal_flap, line_color='red',
                                        line_width=3,
                                        legend='tfinal_flap edge')
            fig.line(self.r, self.tfinal_edge, line_color='blue',
                                        line_width=3,
                                        legend='tfinal_edge')
        except:
            pass

        return fig
